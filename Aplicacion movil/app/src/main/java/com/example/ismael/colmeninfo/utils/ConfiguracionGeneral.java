package com.example.ismael.colmeninfo.utils;


public class ConfiguracionGeneral {

    public static final String TIEMPO_ENTRE_ACTUALIZACIONES_FEED = "tiempoEntreActualizacionesFeed";
    public static final int DEFAULT_TIEMPO_ENTRE_ACTUALIZACION_FEED = 5000;

    public static final String TIEMPO_ENTRE_ACTUALIZACIONES_COLMENA = "tiempoEntreActualizacionesColmena";
    public static final int DEFAULT_TIEMPO_ENTRE_ACTUALIZACION_COLMENA = 3600000;

    public static final String LIMIT_ENTITY_QUERY_HONEYCOMB = "LIMIT_ENTITY_QUERY_HONEYCOMB";
    public static final int DEFAULT_LIMIT_ENTITY_QUERY_HONEYCOMB = 10;

    public static final String ALLOW_UPLOAD_HONEYCOMB_DATA = "ALLOW_UPLOAD_HONEYCOMB_DATA";
    public static final Boolean DEFAULT_ALLOW_UPLOAD_HONEYCOMB_DATA = Boolean.TRUE;

}
