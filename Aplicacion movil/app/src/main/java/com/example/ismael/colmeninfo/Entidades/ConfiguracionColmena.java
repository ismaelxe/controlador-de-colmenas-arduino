package com.example.ismael.colmeninfo.Entidades;

import java.io.Serializable;

public class ConfiguracionColmena implements Serializable {

    private long id;
    private long colmenaId;
    private boolean isActiveVibrationSensor;
    private boolean isActiveTemperatureSensor;
    private boolean isActiveHumiditySensor;

    private double tempMax;
    private double tempMin;
    private double humidityMax;
    private double humidityMin;
    private double lowBatery;


    public ConfiguracionColmena(){
        id = -1;
        isActiveHumiditySensor = true;
        isActiveTemperatureSensor = true;
        isActiveVibrationSensor = true;
        tempMax = 25;
        tempMin = 5;
        humidityMax = 80;
        humidityMin= 20;
        lowBatery = 20;

    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getColmenaId() {
        return colmenaId;
    }

    public void setColmenaId(long colmenaId) {
        this.colmenaId = colmenaId;
    }

    public boolean isActiveVibrationSensor() {
        return isActiveVibrationSensor;
    }

    public void setActiveVibrationSensor(boolean activeVibrationSensor) {
        isActiveVibrationSensor = activeVibrationSensor;
    }

    public boolean isActiveTemperatureSensor() {
        return isActiveTemperatureSensor;
    }

    public void setActiveTemperatureSensor(boolean activeTemperatureSensor) {
        isActiveTemperatureSensor = activeTemperatureSensor;
    }

    public boolean isActiveHumiditySensor() {
        return isActiveHumiditySensor;
    }

    public void setActiveHumiditySensor(boolean activeHumiditySensor) {
        isActiveHumiditySensor = activeHumiditySensor;
    }

    public double getTempMax() {
        return tempMax;
    }

    public void setTempMax(double tempMax) {
        this.tempMax = tempMax;
    }

    public double getTempMin() {
        return tempMin;
    }

    public void setTempMin(double tempMin) {
        this.tempMin = tempMin;
    }

    public double getHumidityMax() {
        return humidityMax;
    }

    public void setHumidityMax(double humidityMax) {
        this.humidityMax = humidityMax;
    }

    public double getHumidityMin() {
        return humidityMin;
    }

    public void setHumidityMin(double humidityMin) {
        this.humidityMin = humidityMin;
    }

    public double getLowBatery() {
        return lowBatery;
    }

    public void setLowBatery(double lowBatery) {
        this.lowBatery = lowBatery;
    }

}
