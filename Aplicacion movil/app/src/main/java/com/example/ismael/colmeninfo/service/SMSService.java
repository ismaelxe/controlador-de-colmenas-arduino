package com.example.ismael.colmeninfo.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.telephony.SmsManager;
import android.util.Log;

import com.example.ismael.colmeninfo.utils.Constants;

import com.example.ismael.colmeninfo.Entidades.*;
import com.example.ismael.colmeninfo.Modelo.*;


import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.ismael.colmeninfo.utils.ConfiguracionGeneral.ALLOW_UPLOAD_HONEYCOMB_DATA;
import static com.example.ismael.colmeninfo.utils.ConfiguracionGeneral.DEFAULT_ALLOW_UPLOAD_HONEYCOMB_DATA;
import static com.example.ismael.colmeninfo.utils.ConfiguracionGeneral.DEFAULT_TIEMPO_ENTRE_ACTUALIZACION_COLMENA;
import static com.example.ismael.colmeninfo.utils.ConfiguracionGeneral.DEFAULT_TIEMPO_ENTRE_ACTUALIZACION_FEED;
import static com.example.ismael.colmeninfo.utils.ConfiguracionGeneral.TIEMPO_ENTRE_ACTUALIZACIONES_COLMENA;
import static com.example.ismael.colmeninfo.utils.ConfiguracionGeneral.TIEMPO_ENTRE_ACTUALIZACIONES_FEED;

public class SMSService extends Service {

    private static final  String tag = "SMSService";

    private Context context;
    private SmsManager smsManager;
    private DBMensajes mensajeDao;

    private SharedPreferences preferences;

    public SMSService(Context context){
        this.context = context;
        smsManager = SmsManager.getDefault();
    }
    public SMSService(){
        context = this;
        smsManager = SmsManager.getDefault();
    }


    public void sendSmsUploadHoneycomb(List<Colmena> colmenas){
        smsManager = SmsManager.getDefault();
        mensajeDao = new DBMensajes(context);
        Mensaje mensaje = new Mensaje();
        String datosColmena = "T:? H:? B:?";
        String datosConfiguracionColmena = "";

        String mensajeColmena = datosColmena + Constants.WHITE_SPACE + datosConfiguracionColmena + Constants.WHITE_SPACE;

        mensaje.setEsRecbido(false);
        mensaje.setMensaje(mensajeColmena);

        for(Colmena colmena : colmenas) {
            try {
                mensaje.setFecha(Calendar.getInstance().getTime());
                mensaje.setColmenaID(colmena.getId());
                mensaje.setCachedStatus(Mensaje.CachedStatus.PROCCESSED);
                mensaje.setEsRecbido(false);
                mensaje.setCachedStatus(Mensaje.CachedStatus.SENT);
                smsManager.sendTextMessage(colmena.getCodPais() + colmena.getTelefono(), null, mensajeColmena, null, null);
            }catch (Exception e){
                Log.e(tag, "Error al procesar el mensaje enviado", e);
                mensaje.setCachedStatus(Mensaje.CachedStatus.ERROR);
            }finally {
                mensajeDao.save(mensaje);
            }
        }

        mensajeDao.close();


    }

    public void sendSmsConfigHoneycomb(Colmena colmena){
        ConfiguracionColmena configuracionColmena = colmena.getConfiguracionColmena();
        Mensaje mensaje = new Mensaje();
        StringBuilder smsBuilder = new StringBuilder();
        //sensores
        smsBuilder.append("CT:" + (configuracionColmena.isActiveTemperatureSensor()?"1":"0"));
        smsBuilder.append(Constants.WHITE_SPACE);
        smsBuilder.append("CH:" + (configuracionColmena.isActiveHumiditySensor()?"1":"0"));
        smsBuilder.append(Constants.WHITE_SPACE);
        smsBuilder.append("CV:" + (configuracionColmena.isActiveVibrationSensor()?"1":"0"));
        smsBuilder.append(Constants.WHITE_SPACE);

        //Maximos y minimos
            smsBuilder.append("SMinT:" + configuracionColmena.getTempMin());
        smsBuilder.append(Constants.WHITE_SPACE);
        smsBuilder.append("SMaxT:" + configuracionColmena.getHumidityMax());
        smsBuilder.append(Constants.WHITE_SPACE);
        smsBuilder.append("SMinH:" + configuracionColmena.getHumidityMin());
        smsBuilder.append(Constants.WHITE_SPACE);
        smsBuilder.append("SMaxH:" + configuracionColmena.getHumidityMax());
        smsBuilder.append(Constants.WHITE_SPACE);
        smsBuilder.append("SlowB:" + configuracionColmena.getLowBatery());
        smsBuilder.append(Constants.WHITE_SPACE);

        //Creamos el mensaje
        mensajeDao = new DBMensajes(context);
        mensaje.setFecha(Calendar.getInstance().getTime());
        mensaje.setColmenaID(colmena.getId());
        mensaje.setCachedStatus(Mensaje.CachedStatus.PROCCESSED);
        mensaje.setEsRecbido(false);
        mensaje.setCachedStatus(Mensaje.CachedStatus.SENT);
        mensajeDao.save(mensaje);
        mensajeDao.close();

        smsManager.sendTextMessage(colmena.getCodPais() + colmena.getTelefono(), null, smsBuilder.toString(), null, null);


    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        preferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        Timer tm = new Timer();
        tm.schedule(new TimerTask() {
            @Override
            public void run() {
                if (preferences.getBoolean(ALLOW_UPLOAD_HONEYCOMB_DATA, DEFAULT_ALLOW_UPLOAD_HONEYCOMB_DATA)) {
                    DBColmenas colmenaDao = new DBColmenas(context);
                    List<Colmena> colmenas = colmenaDao.findHoneyCombs();
                    colmenaDao.close();
                    sendSmsUploadHoneycomb(colmenas);

                    //Voy a guardar un mensaje para hacer la prueba

//                if(!colmenas.isEmpty()){
//                    Colmena colmena = colmenas.get(0);
//                    DBMensajes mensajeDao = new DBMensajes(context);
//                    Mensaje mensaje = new Mensaje();
//                    mensaje.setColmenaID(Long.parseLong(colmena.getId()));
//                    mensaje.setFecha(Calendar.getInstance().getTime());
//                    mensaje.setEsRecbido(false);
//                    mensaje.setCachedStatus(Mensaje.CachedStatus.SENT);
//                    mensaje.setMensaje(String.valueOf(Calendar.getInstance().get(Calendar.MINUTE)));
//                    mensajeDao.save(mensaje);
//                    mensajeDao.close();
//                }
                }
            }
        }, 1000, preferences.getInt(TIEMPO_ENTRE_ACTUALIZACIONES_COLMENA, DEFAULT_TIEMPO_ENTRE_ACTUALIZACION_COLMENA));


        return START_STICKY;
    }
}
