package com.example.ismael.colmeninfo.Modelo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DBNAME = "COLMENASDB";
    private static final int VERSION = 28;

    private static final String SQLCREATECOLMENAS = "create table COLMENAS(" +
            "_id INTEGER primary key AUTOINCREMENT, NOMBRE TEXT, DESCRIPCION TEXT, " +
            "IMAGENURL TEXT, COD_PAIS TEXT, TELEFONO TEXT UNIQUE, LATITUD TEXT, LONGITUD TEXT, TEMPERATURA DOUBLE, HUMEDAD DOUBLE, BATERIA INTEGER);";

    //RECIBIDO --> indica si el mensaje te lo han enviado (true) o si por el contrario lo has enviado tu (false)
    //LEIDO --> indica si el mensaje ha sido procesado por la app (true) o no (false)
    private static final String SQLCREATESMS = "CREATE TABLE MENSAJES(" +
            "_id INTEGER PRIMARY KEY AUTOINCREMENT, COLMENA_ID INTEGER, MENSAJE_TEXT TEXT, FECHA LONG, ES_RECIBIDO INTEGER, LEIDO INTEGER)";

    private static  final String SQLCREATECOLMENASCONFIGURACION = "CREATE TABLE CONFIGURACION_COLMENAS(" +
            "_id INTEGER primary key AUTOINCREMENT, colmena_id INTEGER, isActiveVibrationSensor INTEGER, isActiveTemperatureSensor INTEGER, " +
            "isActiveHumiditySensor INTEGER, temp_max DOUBLE, temp_min DOUBLE, humidity_max DOUBLE, humidity_min DOUBLE, low_batery DOUBLE)";

    private static final String SQL_CREATE_ALERTA = "create table ALERTAS(" +
            "_id INTEGER primary key autoincrement, colmena_id INTEGER, type STRING, fecha LONG, status INTEGER)";


    public DBHelper(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, DBNAME, factory, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQLCREATECOLMENAS);
        sqLiteDatabase.execSQL(SQLCREATESMS);
        sqLiteDatabase.execSQL(SQLCREATECOLMENASCONFIGURACION);
        sqLiteDatabase.execSQL(SQL_CREATE_ALERTA);

//        insertData(sqLiteDatabase);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //Se elimina la versión anterior de la tabla
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS COLMENAS");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS MENSAJES");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS CONFIGURACION_COLMENAS");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS ALERTAS");

//        //Se crea la nueva versión de la tabla
//        sqLiteDatabase.execSQL(SQLCREATECOLMENAS);
//        sqLiteDatabase.execSQL(SQLCREATESMS);
//
//
//        insertData(sqLiteDatabase);
        onCreate(sqLiteDatabase);
    }

    private void insertData(SQLiteDatabase sqLiteDatabase){
        String INSERTCOOLMENA = "INSERT INTO COLMENAS (NOMBRE, DESCRIPCION, IMAGENURL,  COD_PAIS , TELEFONO, LATITUD, LONGITUD, TEMPERATURA, HUMEDAD)" +
                "VALUES ('COLMENA1', 'PRIMERA COLMENA', 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Ruche_artificielle.jpg/1200px-Ruche_artificielle.jpg', '+34' , '634628705', '514', '874', 15, 40)";

        String INSERTSMS = "INSERT INTO MENSAJES(TELEFONO, MENSAJE, FECHA, ES_RECIBIDO, LEIDO) " +
                "VALUES ('634628705', 'ST:0 T:15 H:40', '2019-01-14', 1, 0)";

        sqLiteDatabase.execSQL(INSERTCOOLMENA);
        sqLiteDatabase.execSQL(INSERTSMS);

    }






}
