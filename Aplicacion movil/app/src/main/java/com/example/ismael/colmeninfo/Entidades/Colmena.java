package com.example.ismael.colmeninfo.Entidades;


import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Colmena implements Serializable {
//campos db
    private Integer id;
    private String nombre;
    private String descripcion;
    private String imagen;
    private String codPais;
    private String telefono; //Telefono de la colmena
    private String latitud, longitud;
    private double temperatura, humedad;
    private int bateria;
    //
    private ConfiguracionColmena configuracionColmena;
    private List<Mensaje> mensajes;
    private List<Alerta> alertas;




    public Colmena() {
        id = null;
        nombre = null;
        descripcion = null;
        codPais = null;
        telefono = null;
        latitud = null;
        longitud = null;
        temperatura = 999;
        humedad = -1;
        bateria = 0;

        configuracionColmena = new ConfiguracionColmena();
        mensajes = new ArrayList<>();
        alertas = new ArrayList<>();

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getCodPais() {
        return codPais;
    }

    public void setCodPais(String codPais) {
        this.codPais = codPais;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getHumedad() {
        return humedad;
    }

    public void setHumedad(double humedad) {
        this.humedad = humedad;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public int getBateria() {
        return bateria;
    }

    public void setBateria(int bateria) {
        this.bateria = bateria;
    }

    public ConfiguracionColmena getConfiguracionColmena() {
        return configuracionColmena;
    }

    public void setConfiguracionColmena(ConfiguracionColmena configuracionColmena) {
        this.configuracionColmena = configuracionColmena;
    }

    public List<Mensaje> getMensajes() {
        return mensajes;
    }

    public void setMensajes(List<Mensaje> mensajes) {
        this.mensajes = mensajes;
    }

    public List<Alerta> getAlertas() {
        return alertas;
    }

    public void setAlertas(List<Alerta> alertas) {
        this.alertas = alertas;
    }


    public boolean isAnyAlertActived(){
        for(Alerta alerta : alertas){
            if(alerta.getStatus().equals(Alerta.AlertStatus.ALERT)){
                return true;
            }
        }
        return false;
    }
}
