package com.example.ismael.colmeninfo.Adaptadores;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ismael.colmeninfo.Entidades.Alerta;
import com.example.ismael.colmeninfo.Entidades.Colmena;
import com.example.ismael.colmeninfo.Modelo.DBColmenas;
import com.example.ismael.colmeninfo.R;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class AdaptadorAlertas extends RecyclerView.Adapter<AdaptadorAlertas.ViewHolder> {

    private Context context;
    private List<Alerta> alertas;
    private Map<Integer, Colmena> colmenas;

    public AdaptadorAlertas(Context context, List<Alerta> alertas) {
        this.context = context;
        this.alertas = alertas;
        colmenas = new TreeMap<>();

        DBColmenas colmenaDao = new DBColmenas(context);
        for(Alerta alerta : alertas){
            if(!colmenas.containsKey(alerta.getColmenaId())){
                colmenas.put(alerta.getColmenaId(), colmenaDao.findById(alerta.getColmenaId(), false));
            }
        }
        colmenaDao.close();

    }


    @NonNull
    @Override
    public AdaptadorAlertas.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_alerta, null);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nombreColmena, fechaAlerta, alertType, alertStatus;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nombreColmena = itemView.findViewById(R.id.tvNombreColmenaItemAlerta);
            fechaAlerta = itemView.findViewById(R.id.tvFechaAlertaItemAlerta);
            alertType = itemView.findViewById(R.id.tvAlertTypeItemAlerta);
            alertStatus = itemView.findViewById(R.id.tvAlertStatustemAlerta);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorAlertas.ViewHolder viewHolder, int i) {
        Alerta alerta = alertas.get(i);
        viewHolder.nombreColmena.setText(colmenas.get(alerta.getColmenaId()).getNombre());
        viewHolder.fechaAlerta.setText(alerta.getFecha().toString());
        viewHolder.alertType.setText(alerta.getType().toString());
        viewHolder.alertStatus.setText(alerta.getStatus().toString());
    }

    @Override
    public int getItemCount() {
        return alertas.size();
    }
}
