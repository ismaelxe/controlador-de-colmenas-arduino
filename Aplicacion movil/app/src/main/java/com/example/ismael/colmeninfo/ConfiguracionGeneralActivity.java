package com.example.ismael.colmeninfo;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Switch;


import static com.example.ismael.colmeninfo.utils.ConfiguracionGeneral.*;

public class ConfiguracionGeneralActivity extends AppCompatActivity {
    private static final String TAG ="ConfigGeneralActivity";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private EditText etFeedRefreshTime, etHoneycombRefreshTime;
    private Switch switchAllowUpdateHoneycombs;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion_general);

        sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();


        iniciarViews();

        cargarPreferencias();



    }

    private void cargarPreferencias() {
        etFeedRefreshTime.setText(String.valueOf(sharedPreferences.getInt(TIEMPO_ENTRE_ACTUALIZACIONES_FEED, DEFAULT_TIEMPO_ENTRE_ACTUALIZACION_FEED)));
        etHoneycombRefreshTime.setText(String.valueOf(sharedPreferences.getInt(TIEMPO_ENTRE_ACTUALIZACIONES_COLMENA, DEFAULT_TIEMPO_ENTRE_ACTUALIZACION_COLMENA)));
        switchAllowUpdateHoneycombs.setChecked(sharedPreferences.getBoolean(ALLOW_UPLOAD_HONEYCOMB_DATA, DEFAULT_ALLOW_UPLOAD_HONEYCOMB_DATA));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.configuration_entries_toolbar, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.actionSaveConfigurationEntries: //Guardar preferences
                Log.d(TAG, "Save");
                guardarPreferencias();
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }




    private void iniciarViews() {
        etFeedRefreshTime = findViewById(R.id.etTiempoEntreActualizacionesFeed);
        etHoneycombRefreshTime = findViewById(R.id.etTiempoEntreActualizacionesColmenas);
        switchAllowUpdateHoneycombs = findViewById(R.id.switchAllowUpdateHoneyCombs);
    }

    private void guardarPreferencias(){
        editor.putInt(TIEMPO_ENTRE_ACTUALIZACIONES_FEED, Integer.parseInt(etFeedRefreshTime.getText().toString()));
        editor.putInt(TIEMPO_ENTRE_ACTUALIZACIONES_COLMENA, Integer.parseInt(etHoneycombRefreshTime.getText().toString()));
        editor.putBoolean(ALLOW_UPLOAD_HONEYCOMB_DATA, switchAllowUpdateHoneycombs.isChecked());

        editor.commit();

    }
}
