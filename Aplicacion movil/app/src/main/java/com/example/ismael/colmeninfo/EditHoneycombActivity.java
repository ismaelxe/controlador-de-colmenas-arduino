package com.example.ismael.colmeninfo;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.ismael.colmeninfo.Entidades.Colmena;
import com.example.ismael.colmeninfo.Entidades.ConfiguracionColmena;
import com.example.ismael.colmeninfo.Modelo.DBColmenas;
import com.example.ismael.colmeninfo.service.SMSService;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class EditHoneycombActivity extends AppCompatActivity {
    private static final int REQUEST_GALERIA=2000;

    private EditText etTelefono, etNombre, etDescripcion, etMaxTemp, etMinTemp, etMaxHum, etMinHum, etLowBatery;
    private Colmena colmena;
    private ConfiguracionColmena configuracionColmena;
    private Spinner spCountryCode;
    private ToggleButton tbSensorTemperatura,tbSensorHumedad, tbSensorVibracion;
    private ImageView ivFoto;

    private DBColmenas dbColmenas;
    private SMSService smsService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_honeycomb);

        Intent intent = getIntent();
        colmena = (Colmena) intent.getSerializableExtra("colmena");
        configuracionColmena = colmena.getConfiguracionColmena();

        spCountryCode = findViewById(R.id.spCountryCodeEditHoneycomb);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.country_codes,
                android.R.layout.simple_spinner_item);
        spCountryCode.setAdapter(adapter);

        inicializarViews();
        mostrarDatosColmena();
        smsService = new SMSService(getApplicationContext());
    }


    private void inicializarViews() {
        etTelefono = (EditText) findViewById(R.id.etTelefonoColmena);
        etNombre = (EditText) findViewById(R.id.etNombreColmena);
        etDescripcion = (EditText) findViewById(R.id.etDescripcionColmena);
        tbSensorTemperatura = findViewById(R.id.sensorTempEdtHoneycomb);
        tbSensorHumedad = findViewById(R.id.sensorHumEdtHoneycomb);
        tbSensorVibracion = findViewById(R.id.sensorVibracionEdtHoneycomb);
        etMaxTemp = findViewById(R.id.etMaxTempEditHoneycomb);
        etMinTemp = findViewById(R.id.etMinTempEditHoneycomb);
        etMaxHum = findViewById(R.id.etMaxHumEditHoneycomb);
        etMinHum = findViewById(R.id.etMinHumEditHoneycomb);
        etLowBatery = findViewById(R.id.etLowBateryHoneyComb);
        ivFoto = findViewById(R.id.ivHoneycombImageEdit);

    }

    private void mostrarDatosColmena(){
        etTelefono.setText(colmena.getTelefono());
        etNombre.setText(colmena.getNombre());
        etDescripcion.setText(colmena.getDescripcion());
        tbSensorTemperatura.setChecked(configuracionColmena.isActiveTemperatureSensor());
        tbSensorHumedad.setChecked(configuracionColmena.isActiveHumiditySensor());
        tbSensorVibracion.setChecked(configuracionColmena.isActiveVibrationSensor());
        etMaxTemp.setText(String.valueOf(configuracionColmena.getTempMax()));
        etMinTemp.setText(String.valueOf(configuracionColmena.getTempMin()));
        etMaxHum.setText(String.valueOf(configuracionColmena.getHumidityMax()));
        etMinHum.setText(String.valueOf(configuracionColmena.getHumidityMin()));
        etLowBatery.setText(String.valueOf(configuracionColmena.getLowBatery()));
        if(colmena.getImagen() != null){
            Uri foto = Uri.fromFile(new File(colmena.getImagen()));
            Picasso.with(this).load(foto).into(ivFoto);
        }else{
            Picasso.with(this).load(R.drawable.imagen_colmena_default).into(ivFoto);
        }
    }


    public void seleccionarFotoGaleria(View view){
        Intent photoPickerIntent=new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult( photoPickerIntent,REQUEST_GALERIA);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode==RESULT_OK){

            if(requestCode==REQUEST_GALERIA){
                String path     = "";
                Uri selectedImage=data.getData();
                path = getRealPathFromURI(selectedImage);
                File photoFile = new File(path);
                Picasso.with(this).load(selectedImage).into(ivFoto);
                colmena.setImagen(path);
            }


        }

    }

    public String getRealPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if(cursor.moveToFirst()){;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    public void guardarCambios(View view){

        colmena.setNombre(etNombre.getText().toString());
        colmena.setDescripcion(etDescripcion.getText().toString());
        colmena.setTelefono(etTelefono.getText().toString());
        colmena.setCodPais(spCountryCode.getSelectedItem().toString());
        configuracionColmena.setActiveTemperatureSensor(tbSensorTemperatura.isChecked());
        configuracionColmena.setActiveHumiditySensor(tbSensorHumedad.isChecked());
        configuracionColmena.setActiveVibrationSensor(tbSensorVibracion.isChecked());
        configuracionColmena.setTempMax(Double.parseDouble(etMaxTemp.getText().toString()));
        configuracionColmena.setTempMin(Double.parseDouble(etMinTemp.getText().toString()));
        configuracionColmena.setHumidityMax(Double.parseDouble(etMaxHum.getText().toString()));
        configuracionColmena.setHumidityMin(Double.parseDouble(etMinHum.getText().toString()));
        configuracionColmena.setLowBatery(Double.parseDouble(etLowBatery.getText().toString()));

         dbColmenas = new DBColmenas(this);
        int i = (int) dbColmenas.save(colmena);
        dbColmenas.close();
        if(i == 1){
            smsService.sendSmsConfigHoneycomb(colmena);
            Log.d("guardarCambiosEdit", i +"");
            Toast.makeText(this, "Guardado correcto", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
