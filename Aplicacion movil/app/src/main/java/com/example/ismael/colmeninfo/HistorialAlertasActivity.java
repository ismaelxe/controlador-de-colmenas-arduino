package com.example.ismael.colmeninfo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import com.example.ismael.colmeninfo.Adaptadores.AdaptadorAlertas;
import com.example.ismael.colmeninfo.DAO.AlertaDao;
import com.example.ismael.colmeninfo.Entidades.Alerta;

import java.util.ArrayList;
import java.util.List;

public class HistorialAlertasActivity extends AppCompatActivity {

    private AlertaDao alertaDao;
    private List<Alerta> alertas;

    private RecyclerView rvHistorialAlertas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_alertas);


        alertaDao = new AlertaDao(getApplicationContext());
        alertas = alertaDao.findAll();
        alertaDao.close();

        rvHistorialAlertas = findViewById(R.id.rvHistorialAlertas);

        AdaptadorAlertas adapter = new AdaptadorAlertas(getApplicationContext(), alertas);

        rvHistorialAlertas.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvHistorialAlertas.setAdapter(adapter);

    }
}
