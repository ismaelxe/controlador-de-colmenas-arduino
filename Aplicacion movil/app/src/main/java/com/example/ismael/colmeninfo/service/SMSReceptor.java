package com.example.ismael.colmeninfo.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsMessage;
import android.util.Log;

import com.example.ismael.colmeninfo.DAO.AlertaDao;
import com.example.ismael.colmeninfo.Entidades.Alerta;
import com.example.ismael.colmeninfo.Entidades.Colmena;
import com.example.ismael.colmeninfo.Entidades.Mensaje;
import com.example.ismael.colmeninfo.Modelo.DBColmenas;
import com.example.ismael.colmeninfo.Modelo.DBMensajes;

import java.util.ArrayList;
import java.util.Calendar;

public class SMSReceptor extends BroadcastReceiver {
    private final static String logTag = "SMSReceptor";

    private ArrayList numerosValidos;
    private DBMensajes mensajeDao;
    private DBColmenas dbColmenas;
    private AlertaDao alertaDao;
    String codPais, telefono, mensaje;
    private Colmena colmena;

    @Override
    public void onReceive(Context context, Intent intent) {
        mensajeDao = new DBMensajes(context);
        dbColmenas = new DBColmenas(context);
        alertaDao = new AlertaDao(context);
        numerosValidos = dbColmenas.findHoneycombCodPaisAndNumberPhone();

        telefono = null;
        mensaje = "";

        try {
            //los SMS se envían en trozos que forman un array de objetos el conjunto de trozos se llama pdus
            Object[] pdus = (Object[]) intent.getExtras().get("pdus");
            //recorremos los trozos para montar el mensaje completo
            readSMS(pdus);

            Mensaje sms = new Mensaje();
            //Comprobamos que el numero sea de alguna de nuestras colmenas
            if (numerosValidos.contains(telefono)) {
                //En caso afirmativo buscamos la colmena, esta parte se puede mejorar
                String formatNumber = PhoneNumberUtils.formatNumberToRFC3966(telefono, null);  //tel:+34-634-62-87-05

                if (formatNumber != null) {


                    String[] telefonoSplit = formatNumber.split("-");
                    String numeroTelefono = "";/*Arrays.copyOfRange(telefonoSplit, 1, telefonoSplit.length - 1).toString();*/
                    for (int i = 1; i < telefonoSplit.length; i++) {
                        numeroTelefono += telefonoSplit[i];
                    }


                    colmena = dbColmenas.findByTelefono(numeroTelefono);
                    if (colmena != null) {
                        //Creamos el mensaje y lo guardamos
                        sms.setColmenaID(colmena.getId());
                        sms.setMensaje(mensaje);
                        sms.setFecha(Calendar.getInstance().getTime());
                        sms.setEsRecbido(true);
                        sms.setCachedStatus(Mensaje.CachedStatus.PROCCESSED);
                        mensajeDao.save(sms);

                        //Sacamos los datos del mensaje
                        String datos[] = mensaje.split(" ");

                        updateHoneycombByData(datos);

                        dbColmenas.save(colmena);
                        Log.d(logTag, sms.getColmenaID() + " | " + sms.getMensaje());
                    }
                } else {
                    Log.e("SMSReceptor", "No se ha podido formatear el numero de telefono");
                }

            }
        } catch (Exception e) {
            Log.e(logTag, "Error al procesar el mensaje" + e);
        } finally {
            dbColmenas.close();
            mensajeDao.close();
            alertaDao.close();
        }


    }

    private void updateHoneycombByData(String[] datos) {
        for (String dato : datos) {
            String[] splitDeDato;
            splitDeDato = dato.split(":");

            switch (splitDeDato[0]) {
                case "T":
                    colmena.setTemperatura(Double.parseDouble(splitDeDato[1]));
                    break;
                case "H":
                    colmena.setHumedad(Double.parseDouble(splitDeDato[1]));
                    break;
                case "B":
                    colmena.setBateria(Integer.parseInt(splitDeDato[1]));
                    break;

                //sensores
                case "CV":
                    colmena.getConfiguracionColmena().setActiveVibrationSensor(splitDeDato[1].equals("1"));
                    break;
                case "CT":
                    colmena.getConfiguracionColmena().setActiveTemperatureSensor(splitDeDato[1].equals("1"));
                    break;
                case "CH":
                    colmena.getConfiguracionColmena().setActiveHumiditySensor(splitDeDato[1].equals("1"));
                    break;

                //Minimos y maximos
                case "SMinT":
                    colmena.getConfiguracionColmena().setTempMin(Double.parseDouble(splitDeDato[1]));
                    break;
                case "SMaxT":
                    colmena.getConfiguracionColmena().setTempMax(Double.parseDouble(splitDeDato[1]));
                    break;
                case "SMinH":
                    colmena.getConfiguracionColmena().setHumidityMin(Double.parseDouble(splitDeDato[1]));
                    break;
                case "SMaxH":
                    colmena.getConfiguracionColmena().setHumidityMax(Double.parseDouble(splitDeDato[1]));
                    break;
                case "SlowB":
                    colmena.getConfiguracionColmena().setLowBatery(Double.parseDouble(splitDeDato[1]));
                    break;

                    //Alertas
                case "A":  //Alerta
                    crearAlerta(colmena, splitDeDato[1]);  //switch con los typos de alerta
                    break;

                default:
                    Log.d("SMSReceptorDefault", splitDeDato[0] + " || " + splitDeDato[1]);
            }
            Log.d("SMSReceptor", splitDeDato[0] + " || " + splitDeDato[1]);
        }
    }

    private void crearAlerta(Colmena colmena, String codAlerta) {
        Alerta alerta = new Alerta();
        alerta.setColmenaId(colmena.getId());
        alerta.setFecha(Calendar.getInstance().getTime());
        alerta.setStatus(Alerta.AlertStatus.ALERT);
        switch (codAlerta){
            case "V":
                alerta.setType(Alerta.AlertType.VibracionAlert);
                break;
            case "TX":
                alerta.setType(Alerta.AlertType.MaxTemperaturaAlert);
                break;
            case "TN":
                alerta.setType(Alerta.AlertType.MinTemperaturaAlert);
                break;
            case "HX":
                alerta.setType(Alerta.AlertType.MaxHumedadAlert);
                break;
            case "HN":
                alerta.setType(Alerta.AlertType.MinHumedadAlert);
                break;
            case "BN":
                alerta.setType(Alerta.AlertType.MinBateriaAlert);
                break;

                default:
                    alerta.setType(Alerta.AlertType.NONE);

        }

         alertaDao.save(alerta);

    }

    private void readSMS(Object[] pdus) {
        for (int i = 0; i < pdus.length; i++) {
            //obtenemos el objeto SmsMessage de cada pdu
            SmsMessage sm = SmsMessage.createFromPdu(
                    (byte[]) pdus[i], "3gpp");

            //recuperamos el número desde el que se ha enviado el mensaje
            if (telefono == null) {
                telefono = sm.getOriginatingAddress();

            }

            //recuperamos el texto de ese trozo de mensaje
            mensaje += sm.getMessageBody();
        }
    }

}
