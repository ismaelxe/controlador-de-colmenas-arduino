package com.example.ismael.colmeninfo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.ismael.colmeninfo.Adaptadores.AdaptadorMensajes;
import com.example.ismael.colmeninfo.Entidades.Mensaje;
import com.example.ismael.colmeninfo.Modelo.DBMensajes;

import java.util.List;

public class HistorialMensajesActivity extends AppCompatActivity {

    private DBMensajes mensajeDao;
    private List<Mensaje> mensajes;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_mensajes);

        initViews();

        mensajeDao = new DBMensajes(getApplicationContext());
        mensajes = mensajeDao.findAll();
        mensajeDao.close();

        AdaptadorMensajes adapter = new AdaptadorMensajes(getApplicationContext(), mensajes);
        recyclerView.setAdapter(adapter);

    }

    private void initViews() {
        recyclerView = findViewById(R.id.rvHistorialMensajes);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }
}
