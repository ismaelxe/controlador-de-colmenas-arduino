package com.example.ismael.colmeninfo.Adaptadores;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ismael.colmeninfo.Entidades.Colmena;
import com.example.ismael.colmeninfo.Entidades.Mensaje;
import com.example.ismael.colmeninfo.Modelo.DBColmenas;
import com.example.ismael.colmeninfo.R;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class AdaptadorMensajes extends RecyclerView.Adapter<AdaptadorMensajes.ViewHolder> {

    private Context context;
    private List<Mensaje> mensajes;
    private Map<Integer, Colmena> colmenas;

    public AdaptadorMensajes(Context context, List<Mensaje> mensajes) {
        this.context = context;
        this.mensajes = mensajes;
        colmenas = new TreeMap<>();

        DBColmenas colmenaDao = new DBColmenas(context);
        for(Mensaje mens : mensajes){
            if(!colmenas.containsKey(mens.getColmenaID())){
                colmenas.put(mens.getColmenaID(), colmenaDao.findById(mens.getColmenaID (), false));
            }
        }
        colmenaDao.close();

    }


    @NonNull
    @Override
    public AdaptadorMensajes.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_mensaje, null);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nombreColmena, fecha, stringMensaje, esRecibido;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nombreColmena = (TextView) itemView.findViewById(R.id.tvNombreColmenaItemMensaje);
            fecha = itemView.findViewById(R.id.tvFechaItemMensaje);
            stringMensaje = itemView.findViewById(R.id.tvMensajeItemMensaje);
            esRecibido = itemView.findViewById(R.id.tvEsRecibidoItemAlerta);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorMensajes.ViewHolder viewHolder, int i) {
        Colmena colmena = colmenas.get(mensajes.get(i).getColmenaID());
        Mensaje mensaje = mensajes.get(i);
        if(colmena.getNombre() != null)viewHolder.nombreColmena.setText(colmena.getNombre());
        viewHolder.fecha.setText(mensaje.getFecha().toString());
        viewHolder.stringMensaje.setText(mensaje.getMensaje());
        viewHolder.esRecibido.setText(mensaje.isEsRecbido()?"<-": "->");  //Cambiar por iconos enviado/recibido
    }

    @Override
    public int getItemCount() {
        return mensajes.size();
    }
}
