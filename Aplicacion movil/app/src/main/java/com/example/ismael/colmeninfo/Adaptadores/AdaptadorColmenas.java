package com.example.ismael.colmeninfo.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import com.example.ismael.colmeninfo.ColmenaActivity;
import com.example.ismael.colmeninfo.Entidades.Colmena;
import com.example.ismael.colmeninfo.R;
import com.squareup.picasso.Picasso;

public class AdaptadorColmenas extends RecyclerView.Adapter<AdaptadorColmenas.ViewHolder> {


    private ArrayList<Colmena> listDatos;
    private Context context;

    public AdaptadorColmenas(Context context, ArrayList<Colmena> list) {
        this.context = context;
        listDatos = list;
        //Log.d("AdaptadorColmenas", list.size()+"");
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_colmena, null, false);

        return new ViewHolder(view);
    }






    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int i) {
        holder.nombre.setText(listDatos.get(i).getNombre());
        holder.descripcion.setText(listDatos.get(i).getDescripcion());
        holder.ivAlerta.setVisibility(listDatos.get(i).isAnyAlertActived()?View.VISIBLE:View.INVISIBLE);
        if(listDatos.get(i).getImagen() != null){
            Uri foto = Uri.fromFile(new File(listDatos.get(i).getImagen()));
            Picasso.with(context).load(foto).into(holder.imagen);
        }else{
            Picasso.with(context).load(R.drawable.imagen_colmena_default).into(holder.imagen);
        }
//        Picasso.with(context).load(listDatos.get(i).getImagen()).into(holder.imagen);
        Log.d("AdaptadorColmenas", listDatos.get(i).getNombre());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ColmenaActivity.class);
                intent.putExtra("colmena", listDatos.get(i));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        Log.d("AdaptadorColmenas", listDatos.size()+"");
        return listDatos.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nombre, descripcion;
        ImageView imagen, ivAlerta;
        CardView cardView;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nombre = (TextView) itemView.findViewById(R.id.tvTNombreItemMain);
            descripcion = (TextView) itemView.findViewById(R.id.tvDescripcionItemMain);
            imagen = (ImageView) itemView.findViewById(R.id.ivItemMain);
            ivAlerta = itemView.findViewById(R.id.ivItemAlertMain);
            cardView = (CardView) itemView.findViewById(R.id.cwItemMain);

            nombre.setTextSize(20);
            nombre.setTextColor(itemView.getResources().getColor(android.R.color.black));
            nombre.setBackgroundColor(itemView.getResources().getColor(android.R.color.white));

            descripcion.setTextColor(itemView.getResources().getColor(android.R.color.black));

            ivAlerta.setColorFilter(Color.parseColor("red"));

        }
    }


}
