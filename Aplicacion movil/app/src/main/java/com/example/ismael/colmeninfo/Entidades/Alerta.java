package com.example.ismael.colmeninfo.Entidades;

import java.io.Serializable;
import java.util.Date;

public class Alerta implements Serializable {

    private int colmenaId;
    private int id;
    private Date fecha;
    private AlertType type;
    private AlertStatus status;


    public Alerta(){
        id = -1;
        colmenaId = -1;
        fecha = null;
        type = null;
        status = null;
    }

    public int getColmenaId() {
        return colmenaId;
    }

    public void setColmenaId(int colmenaId) {
        this.colmenaId = colmenaId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public AlertType getType() {
        return type;
    }

    public void setType(AlertType type) {
        this.type = type;
    }

    public AlertStatus getStatus() {
        return status;
    }

    public void setStatus(AlertStatus status) {
        this.status = status;
    }

    public enum AlertType{
        NONE,
        VibracionAlert,
        MaxTemperaturaAlert,
        MinTemperaturaAlert,
        MaxHumedadAlert,
        MinHumedadAlert,
        MinBateriaAlert
    }

    public enum AlertStatus {
        NONE,
        ALERT,
        READ
    }

}
