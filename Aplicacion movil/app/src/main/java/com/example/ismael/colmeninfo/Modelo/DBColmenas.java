package com.example.ismael.colmeninfo.Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.ismael.colmeninfo.DAO.AlertaDao;
import com.example.ismael.colmeninfo.DAO.ConfiguracionColmenaDao;
import com.example.ismael.colmeninfo.Entidades.Colmena;
import com.example.ismael.colmeninfo.Entidades.ConfiguracionColmena;

import java.util.ArrayList;

import static com.example.ismael.colmeninfo.utils.ConfiguracionGeneral.DEFAULT_LIMIT_ENTITY_QUERY_HONEYCOMB;

public class DBColmenas {
    private SQLiteDatabase db = null;
    private DBHelper dbHelper = null;
    private Context context;
    private String[] fields = {"_id","NOMBRE", "DESCRIPCION" ,
            "IMAGENURL" , "COD_PAIS", "TELEFONO" , "LATITUD" , "LONGITUD", "TEMPERATURA", "HUMEDAD", "BATERIA"};

    private ConfiguracionColmenaDao configuracionColmenaDao;
    private DBMensajes mensajeDao;
    private AlertaDao alertaDao;

    public DBColmenas(Context context){
        this.context = context;
        dbHelper = new DBHelper(context, null);
        db = dbHelper.getWritableDatabase();
        configuracionColmenaDao = new ConfiguracionColmenaDao(context);
        mensajeDao = new DBMensajes(context);
        alertaDao = new AlertaDao(context);
    }

    public void close(){
        dbHelper.close();
        configuracionColmenaDao.close();
        mensajeDao.close();
        alertaDao.close();
    }

    public Colmena findById(int id){
        return findById(Long.parseLong(String.valueOf(id)));
    }
    public Colmena findById(long id){
        Cursor c = db.query("COLMENAS", fields, "_id=?",
                new String[]{String.valueOf(id)}, null, null, null);

        if(c.moveToFirst()){
            Colmena colmena = makeCursorToColmena(c);
            c.close();

            findExtraData(colmena);

            return colmena;
        }
        return null;
    }

    public Colmena findById(int id, boolean findExtraData){
        Cursor c = db.query("COLMENAS", fields, "_id=?",
                new String[]{String.valueOf(id)}, null, null, null);

        if(c.moveToFirst()){
            Colmena colmena = makeCursorToColmena(c);
            c.close();

            if(findExtraData) {
                findExtraData(colmena);
            }

            return colmena;
        }
        return null;
    }



    public Colmena findByTelefono(String telefono){
        Cursor c = db.query("COLMENAS", fields, "TELEFONO=?",
                new String[]{telefono}, null, null, null);

        if(c.moveToNext()){
            Colmena colmena = makeCursorToColmena(c);
            c.close();

            findExtraData(colmena);

            return colmena;
        }
        return null;
    }



    public ArrayList<Colmena> findHoneyCombs(){
        ArrayList<Colmena> colmenas = new ArrayList<>();

        Cursor c = db.query("COLMENAS", fields, null,
                null, null, null, null);
        Colmena colmena;
        while (c.moveToNext()){
            colmena = makeCursorToColmena(c);
            findExtraData(colmena);
            colmenas.add(colmena);
        }
        c.close();

        return colmenas;
    }

    public long save(Colmena colmena){
        ContentValues values = new ContentValues();
        values.put("NOMBRE", colmena.getNombre());
        values.put("DESCRIPCION", colmena.getDescripcion());
        values.put("TELEFONO", colmena.getTelefono());
        values.put("COD_PAIS", colmena.getCodPais());
        values.put("IMAGENURL", colmena.getImagen());
        values.put("TEMPERATURA", colmena.getTemperatura());
        values.put("HUMEDAD", colmena.getHumedad());
        values.put("BATERIA", colmena.getBateria());


        if(colmena.getId() != null && !(colmena.getId()==-1)) {
            configuracionColmenaDao.save(colmena.getConfiguracionColmena());
            if(colmena.getAlertas() != null && !colmena.getAlertas().isEmpty())alertaDao.save(colmena.getAlertas());

            return db.update("COLMENAS", values,"_id=?", new String[]{String.valueOf(colmena.getId())});
        }else{
            long idColmena = db.insert("COLMENAS", null, values);
            ConfiguracionColmena configuracionColmena = colmena.getConfiguracionColmena();
            configuracionColmena.setColmenaId(idColmena);
            configuracionColmenaDao.save(configuracionColmena);
            return idColmena;
        }


    }

    public int removeById(String id){
        configuracionColmenaDao.deleteConfiguracionColmenaById(id);
        return db.delete("COLMENAS", "_id = ?", new String[]{id});
    }

    //Este metodo esta obsoleto
    public int updateHoneycombById(Colmena colmena){
        ContentValues values = new ContentValues();
        values.put("NOMBRE", colmena.getNombre());
        values.put("DESCRIPCION", colmena.getDescripcion());
        values.put("TELEFONO", colmena.getTelefono());
        values.put("COD_PAIS", colmena.getCodPais());
        values.put("IMAGENURL", colmena.getImagen());
        values.put("TEMPERATURA", colmena.getTemperatura());
        values.put("HUMEDAD", colmena.getHumedad());

        configuracionColmenaDao.save(colmena.getConfiguracionColmena());

        return db.update("COLMENAS", values,"_id=?", new String[]{String.valueOf(colmena.getId())});
    }

    public ArrayList<String> findHoneycombCodPaisAndNumberPhone(){
        ArrayList<String> numbers = new ArrayList<>();
        Cursor c = db.query("COLMENAS", new String[]{"COD_PAIS","TELEFONO"}, null,null,null,null, null);

        while(c.moveToNext()){
            String codPais = c.getString(0);
            String telefono = c.getString(1);
            String fullPhone = codPais + telefono;

            numbers.add(fullPhone);
            Log.d("DBColmenasPhone", fullPhone);
        }

        return numbers;
    }


    private Colmena makeCursorToColmena(Cursor c) {
        Colmena colmena = new Colmena();

        int idColumn = c.getColumnIndex("_id");
        int nombreColumn = c.getColumnIndex("NOMBRE");
        int descColumn = c.getColumnIndex("DESCRIPCION");
        int imagenColumn = c.getColumnIndex("IMAGENURL");
        int telefonoColumn = c.getColumnIndex("TELEFONO");
        int latitudColumn = c.getColumnIndex("LATITUD");
        int longitudColumn = c.getColumnIndex("LONGITUD");
        int temperaturaColumn = c.getColumnIndex("TEMPERATURA");
        int humedadColumn = c.getColumnIndex("HUMEDAD");
        int codPaisColum = c.getColumnIndex("COD_PAIS");
        int bateriaColumn = c.getColumnIndex("BATERIA");

        colmena.setId(c.getInt(idColumn));
        colmena.setNombre(c.getString(nombreColumn));
        colmena.setDescripcion(c.getString(descColumn));
        colmena.setImagen(c.getString(imagenColumn));
        colmena.setTelefono(c.getString(telefonoColumn));
        colmena.setLatitud(c.getString(latitudColumn));
        colmena.setLongitud(c.getString(longitudColumn));
        colmena.setTemperatura(c.getDouble(temperaturaColumn));
        colmena.setHumedad(c.getDouble(humedadColumn));
        colmena.setCodPais(c.getString(codPaisColum));
        colmena.setBateria(c.getInt(bateriaColumn));

        return colmena;
    }

    private void findExtraData(Colmena colmena) {
        ConfiguracionColmena configuracionColmena = configuracionColmenaDao.findByColmena(colmena);
//        List<Mensaje> mensajes = mensajeDao.findMensajesByColmena(colmena);
        colmena.setConfiguracionColmena(configuracionColmena);
//        colmena.setMensajes(mensajes);
        colmena.setAlertas(alertaDao.findByColmena(colmena, String.valueOf(DEFAULT_LIMIT_ENTITY_QUERY_HONEYCOMB)));
    }
}
