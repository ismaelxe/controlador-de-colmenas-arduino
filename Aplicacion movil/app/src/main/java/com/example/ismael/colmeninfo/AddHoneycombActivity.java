package com.example.ismael.colmeninfo;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.ismael.colmeninfo.Entidades.Colmena;
import com.example.ismael.colmeninfo.Modelo.DBColmenas;

public class AddHoneycombActivity extends AppCompatActivity {
    private Colmena colmena;
    private EditText etNombre, etDescripcion, etTelefono;
    private Spinner spCountryCode;
    private DBColmenas db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_honeycomb);


        etNombre = (EditText)findViewById(R.id.etNombreColmenaAddHoneycomb);
        etDescripcion = (EditText) findViewById(R.id.etDescripcionAddHoneycomb);
        etTelefono = (EditText) findViewById(R.id.etTelefonoAddHoneycomb);
        spCountryCode = findViewById(R.id.spCountryCodeAddHoneycomb);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.country_codes,
                android.R.layout.simple_spinner_item);
        spCountryCode.setAdapter(adapter);


        colmena  = new Colmena();


    }

    //Este metodo guarda la nueva colmena en la base de datos siempre que los datos sean correctos
    public void saveHoneycom(View view){
        if(checkData()){
            db = new DBColmenas(this);
            colmena.setNombre(etNombre.getText().toString());
            colmena.setDescripcion(etDescripcion.getText().toString());
            colmena.setTelefono(etTelefono.getText().toString());
            colmena.setCodPais(spCountryCode.getSelectedItem().toString());

            if(db.save(colmena) != -1){
                db.close();
                Snackbar.make(view, "Colmena añadida", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                finish();
            }


        }else{
            Snackbar.make(view, "Debes completar los campos obligatorios", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
    }

    private boolean checkData() {
        boolean datosCorrectos = true;

        if(etNombre.getText().toString().equals("")){
            datosCorrectos = false;
        }
//        if(etDescripcion.getText().toString().equals("")){
//            datosCorrectos = false;
//        }
        if(etTelefono.getText().toString().equals("")){
            datosCorrectos = false;
        }
        if(spCountryCode.getSelectedItem() == null){
            datosCorrectos = false;
        }

        return datosCorrectos;
    }

    public void checkPhone(View view){} //Comprueba que la colmena existe enviando un mensaje y esperando contestacion
}
