package com.example.ismael.colmeninfo.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.ismael.colmeninfo.Entidades.Colmena;
import com.example.ismael.colmeninfo.Entidades.ConfiguracionColmena;
import com.example.ismael.colmeninfo.Entidades.Mensaje;
import com.example.ismael.colmeninfo.Modelo.DBHelper;

public class ConfiguracionColmenaDao {
    private DBHelper dbHelper;
    private Context context;
    private SQLiteDatabase db;

    private static final String dbName = "CONFIGURACION_COLMENAS";
    private static final String [] dbFields = new String[]{"_id", "colmena_id", "isActiveVibrationSensor", "isActiveTemperatureSensor",
    "isActiveHumiditySensor", "temp_max", "temp_min", "humidity_max", "humidity_min", "low_batery"};

    public ConfiguracionColmenaDao(Context context){
        this.context = context;
        dbHelper = new DBHelper(context, null);
        db = dbHelper.getWritableDatabase();
    }


    public void close(){dbHelper.close();}


    public void save(ConfiguracionColmena configuracionColmena){

        ContentValues values = new ContentValues();
        values.put("colmena_id", configuracionColmena.getColmenaId());
        values.put("isActiveVibrationSensor", configuracionColmena.isActiveVibrationSensor()?1:0);
        values.put("isActiveTemperatureSensor", configuracionColmena.isActiveTemperatureSensor()?1:0);
        values.put("isActiveHumiditySensor", configuracionColmena.isActiveHumiditySensor()?1:0);
        values.put("temp_max", configuracionColmena.getTempMax());
        values.put("temp_min", configuracionColmena.getTempMin());
        values.put("humidity_max", configuracionColmena.getHumidityMax());
        values.put("humidity_min", configuracionColmena.getHumidityMin());
        values.put("low_batery", configuracionColmena.getLowBatery());


        int value;
        if(configuracionColmena.getId() == -1) {
            value = (int) db.insert(dbName, null, values);
        }else{
            value = db.update(dbName,values, "_id=?", new String[]{String.valueOf(configuracionColmena.getId())});
        }
        Log.d("ConfigurationColmenaDAO", "save=" + value);

    }

    public void update(ConfiguracionColmena configuracionColmena){
        ContentValues values = new ContentValues();
        values.put("colmena_id", configuracionColmena.getColmenaId());
        values.put("isActiveVibrationSensor", configuracionColmena.isActiveVibrationSensor()?1:0);
        values.put("isActiveTemperatureSensor", configuracionColmena.isActiveTemperatureSensor()?1:0);
        values.put("isActiveHumiditySensor", configuracionColmena.isActiveHumiditySensor()?1:0);
        values.put("temp_max", configuracionColmena.getTempMax());
        values.put("temp_min", configuracionColmena.getTempMin());
        values.put("humidity_max", configuracionColmena.getHumidityMax());
        values.put("humidity_min", configuracionColmena.getHumidityMin());
        values.put("low_batery", configuracionColmena.getLowBatery());



    }

    public ConfiguracionColmena findByColmena(Colmena colmena) {
        ConfiguracionColmena configuracionColmena;
        Cursor cursor = db.query(dbName, dbFields, "colmena_id=?",
                new String[]{String.valueOf(colmena.getId())}, null, null, null);
        if (cursor.moveToFirst()) {
            configuracionColmena = makeCursorToColmena(cursor);
            cursor.close();

            return configuracionColmena;
        }
        return null;
    }



    private ConfiguracionColmena makeCursorToColmena(Cursor c) {
        try {
            ConfiguracionColmena configuracionColmena = new ConfiguracionColmena();

            int idColumn = c.getColumnIndex("_id");
            int colemanIdColumn = c.getColumnIndex("colmena_id");
            int isActiveVibrationSensorColumn = c.getColumnIndex("isActiveVibrationSensor");
            int isActiveTemperatureSensorColumn = c.getColumnIndex("isActiveTemperatureSensor");
            int isActiveHumiditySensorColumn = c.getColumnIndex("isActiveHumiditySensor");
            int tempMaxColumn = c.getColumnIndex("temp_max");
            int tempMinColumn = c.getColumnIndex("temp_min");
            int humidityMaxColumn = c.getColumnIndex("humidity_max");
            int humidityMinColumn = c.getColumnIndex("humidity_min");
            int lowBateryColumn = c.getColumnIndex("low_batery");

            configuracionColmena.setId(c.getLong(idColumn));
            configuracionColmena.setColmenaId(c.getInt(colemanIdColumn));
            configuracionColmena.setActiveVibrationSensor(c.getInt(isActiveVibrationSensorColumn) == 1);
            configuracionColmena.setActiveTemperatureSensor(c.getInt(isActiveTemperatureSensorColumn) == 1);
            configuracionColmena.setActiveHumiditySensor(c.getInt(isActiveHumiditySensorColumn) == 1);
            configuracionColmena.setTempMax(c.getDouble(tempMaxColumn));
            configuracionColmena.setTempMin(c.getDouble(tempMinColumn));
            configuracionColmena.setHumidityMax(c.getDouble(humidityMaxColumn));
            configuracionColmena.setHumidityMin(c.getDouble(humidityMinColumn));
            configuracionColmena.setLowBatery(c.getDouble(lowBateryColumn));

            return configuracionColmena;
        }catch (Exception e){
            Log.e("ConfiguracionColmenaDao", "Error al convertir el cursor " + e);
            return null;
        }
    }

    public void deleteConfiguracionColmenaById(String id) {
        db.delete(dbName,"colmena_id = ?",new String[]{id});
    }
}
