package com.example.ismael.colmeninfo;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.ismael.colmeninfo.Adaptadores.AdaptadorColmenas;
import com.example.ismael.colmeninfo.Entidades.Colmena;
import com.example.ismael.colmeninfo.Modelo.DBColmenas;
import com.example.ismael.colmeninfo.service.SMSService;

import java.util.ArrayList;

import static com.example.ismael.colmeninfo.utils.ConfiguracionGeneral.*;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final int PERMISSIONS_REQUEST_RECEIVE_SMS = 0;

    private ArrayList<Colmena> colmenas;
    private RecyclerView recyclerView;
    private DBColmenas db;
    private FloatingActionButton fab;
    private SharedPreferences preferences;

    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        crearBotonFlotante();

        loadMenu();

        handler = new Handler();

        recyclerView = (RecyclerView) findViewById(R.id.rvColmenasMain);

        this.startService(new Intent(this,SMSService.class));


        //Comprobamos que tenemos los permisos necesarios y los pedimos en caso contrario
        comprobarPermisos();





    }


    @Override
    protected void onPause() {
        super.onPause();

        colmenas = new ArrayList<>();

        actualizarColmenas();

    }

    //---------------------------------------------------------------------------------------------------------------
    private void comprobarPermisos() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.RECEIVE_SMS, Manifest.permission.SEND_SMS,Manifest.permission.READ_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_RECEIVE_SMS);
        }

    }





    //---------------------------------------------------------------------------------------------------------------


    private void loadMenu() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void crearBotonFlotante() {
        fab = (FloatingActionButton) findViewById(R.id.fabAgregarColmena);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),AddHoneycombActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        preferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        actualizarColmenas();
        actualizarColmenasJob();
    }

    private void actualizarColmenas() {
        db = new DBColmenas(this);
        colmenas = db.findHoneyCombs();
        db.close();

        AdaptadorColmenas adapter = new AdaptadorColmenas(this, colmenas);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), ConfiguracionGeneralActivity.class);
            startActivity(intent);
        }else if(id == R.id.action_refresh){
            SMSService smsService = new SMSService(getApplicationContext());
            smsService.sendSmsUploadHoneycomb(colmenas);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;

        switch (id){
            case R.id.navHistorialAlertas:
                intent = new Intent(this, HistorialAlertasActivity.class);
                startActivity(intent);
                break;

            case R.id.navHistorialMensajes:
                intent = new Intent(this, HistorialMensajesActivity.class);
                startActivity(intent);
                break;

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    public void actualizarColmenasJob() {
        //TODO Permitir activar o desactivar la comprobacion de las colmenas por sms
        handler.postDelayed(new Runnable() {
            public void run() {

                // función a ejecutar
                actualizarColmenas();

                handler.postDelayed(this, preferences.getInt(TIEMPO_ENTRE_ACTUALIZACIONES_FEED, DEFAULT_TIEMPO_ENTRE_ACTUALIZACION_FEED));
            }

        }, preferences.getInt(TIEMPO_ENTRE_ACTUALIZACIONES_FEED, DEFAULT_TIEMPO_ENTRE_ACTUALIZACION_FEED));

    }
}
