package com.example.ismael.colmeninfo.Entidades;

import java.util.Date;

public class Mensaje {
    private String id;
    private Integer colmenaID;
    private String mensaje;
    private Date fecha;
    private boolean esRecbido;
    private CachedStatus cachedStatus;

    public Mensaje(){
        id = null;
        colmenaID = null;
        mensaje = null;
        fecha = new Date();
        esRecbido = false;
        cachedStatus = CachedStatus.NONE;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public boolean isEsRecbido() {
        return esRecbido;
    }

    public void setEsRecbido(boolean esRecbido) {
        this.esRecbido = esRecbido;
    }

    public CachedStatus getCachedStatus() {
        return cachedStatus;
    }

    public void setCachedStatus(CachedStatus cachedStatus) {
        this.cachedStatus = cachedStatus;
    }

    public int getColmenaID() {
        return colmenaID;
    }

    public void setColmenaID(int colmenaID) {
        this.colmenaID = colmenaID;
    }

    public enum CachedStatus  {
        NONE,
        PROCCESSED,
        ERROR,
        SENT
    }
}
