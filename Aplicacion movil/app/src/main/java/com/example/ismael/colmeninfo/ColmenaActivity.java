package com.example.ismael.colmeninfo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ismael.colmeninfo.Adaptadores.AdaptadorMensajes;
import com.example.ismael.colmeninfo.DAO.AlertaDao;
import com.example.ismael.colmeninfo.Entidades.Alerta;
import com.example.ismael.colmeninfo.Entidades.Colmena;
import com.example.ismael.colmeninfo.Entidades.ConfiguracionColmena;
import com.example.ismael.colmeninfo.Entidades.Mensaje;
import com.example.ismael.colmeninfo.Modelo.DBColmenas;
import com.example.ismael.colmeninfo.Modelo.DBMensajes;

import java.util.List;

import static com.example.ismael.colmeninfo.utils.ConfiguracionGeneral.*;

public class ColmenaActivity extends AppCompatActivity {
    private static final String TAG = "ColmenaActivity";

    private Colmena colmena;
    private List<Mensaje> mensajes;
    private TextView tvNombre, tvDescripcion, tvTemperatura, tvHumedad, tvBateria;
    private RecyclerView rvMensajes;
    private ImageView ivAlertaTemp, ivAlertaHum, ivAlertaBat, ivAlertaVibracion;

    private DBMensajes mensajeDao;
    private  DBColmenas colmenasDao;
    private SharedPreferences preferences;
    private AlertaDao alertaDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colmena);

        preferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);

        inicializarViews();
        Intent intent = getIntent();
        colmena = (Colmena) intent.getSerializableExtra("colmena");

        mensajeDao = new DBMensajes(this);
        mensajes = mensajeDao.findMensajesByColmena(colmena.getId(), String.valueOf(preferences.getInt(LIMIT_ENTITY_QUERY_HONEYCOMB, DEFAULT_LIMIT_ENTITY_QUERY_HONEYCOMB)));
        mensajeDao.close();
        mostrarDatosColmena();
        configurarRecyclerViewMensajes();
        marcarAlertasComoLeidas();


    }

    private void marcarAlertasComoLeidas() {
        List<Alerta> alertas = colmena.getAlertas();
        for(Alerta alerta : alertas){
            if(alerta.getStatus().equals(Alerta.AlertStatus.ALERT)){
                alerta.setStatus(Alerta.AlertStatus.READ);
                Log.i(TAG, "Alerta leida");
            }
        }
        colmenasDao = new DBColmenas(getApplicationContext());

        colmenasDao.save(colmena);

        colmenasDao.close();
    }

    private void configurarRecyclerViewMensajes() {
        rvMensajes.setHasFixedSize(true);
        rvMensajes.setLayoutManager(new LinearLayoutManager(this));
        RecyclerView.Adapter adapter = new AdaptadorMensajes(getApplicationContext(), mensajes);

        rvMensajes.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.colmena_toolbar, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.actionEditHoneycomb: //Editar colmena
                Log.d("ActionBar", "Editar!");
                Intent intent = new Intent(getApplicationContext(), EditHoneycombActivity.class);
                intent.putExtra("colmena",colmena);
                startActivity(intent);
                finish();
                return true;

            case R.id.actionDeleteHoneycomb://Eliminar colmena
                AlertDialog.Builder cuadro=new AlertDialog.Builder(this);
                cuadro.setMessage("Seguro de eliminar esta colmena?");

                //En caso afirmativo se elimina la colmena
                cuadro.setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                DBColmenas db = new DBColmenas(getApplicationContext());
                                db.removeById(String.valueOf(colmena.getId()));
                                db.close();
                                finish();
                            }
                        });
                //En caso negativo no se hará nada
                cuadro.setNegativeButton(android.R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                        cuadro.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    private void inicializarViews() {
        tvNombre = (TextView)findViewById(R.id.tvNombreColmena);
        tvDescripcion = (TextView)findViewById(R.id.tvDescripcionColmena);
        tvTemperatura = findViewById(R.id.tvTemperaturaColmena);
        tvHumedad = findViewById(R.id.tvHumedadColmena);
        tvBateria = findViewById(R.id.tvBateriaColmena);
        rvMensajes = findViewById(R.id.rvColmenaMensajes);
        ivAlertaTemp = findViewById(R.id.ivAlertaTemperaturaHoneycomb);
        ivAlertaHum = findViewById(R.id.ivAlertaHumedadHoneycomb);
        ivAlertaBat = findViewById(R.id.ivAlertaBateriaHoneycomb);
        ivAlertaVibracion = findViewById(R.id.ivAlertaVibracionHoneycomb);

    }

    private void mostrarDatosColmena(){
        tvNombre.setText(colmena.getNombre());
        tvDescripcion.setText(colmena.getDescripcion());
        tvTemperatura.setText(String.valueOf(colmena.getTemperatura()));
        tvHumedad.setText(String.valueOf(colmena.getHumedad()));
        tvBateria.setText(String.valueOf(colmena.getBateria()));

        //Comprobamos si hay alertas que mostrar
        mostrarAlertas();

    }

    private void mostrarAlertas() {
        ConfiguracionColmena configuracionColmena = colmena.getConfiguracionColmena();
        if(colmena.getTemperatura() > configuracionColmena.getTempMax()){
            ivAlertaTemp.setVisibility(View.VISIBLE);
            ivAlertaTemp.setImageResource(R.drawable.caliente);
        }
        if(colmena.getTemperatura() < configuracionColmena.getTempMin()){
            ivAlertaTemp.setVisibility(View.VISIBLE);
            ivAlertaTemp.setImageResource(R.drawable.frio);
        }

        if(colmena.getHumedad() > configuracionColmena.getHumidityMax()){
            ivAlertaHum.setVisibility(View.VISIBLE);
        }
        if(colmena.getHumedad() < configuracionColmena.getHumidityMin()){
            ivAlertaHum.setVisibility(View.VISIBLE);
        }

        if(colmena.getBateria() < configuracionColmena.getLowBatery()){
            ivAlertaBat.setVisibility(View.VISIBLE);
        }

        if (shouldShowAlertaVibracion()){
            ivAlertaVibracion.setVisibility(View.VISIBLE);
            ivAlertaVibracion.setImageResource(R.drawable.robo);
        }

    }

    private boolean shouldShowAlertaVibracion() {
        alertaDao = new AlertaDao(getApplicationContext());
        Alerta alerta = alertaDao.findLastByColmenaAndStatus(colmena,Alerta.AlertType.VibracionAlert);

        if(alerta!= null && alerta.getStatus().equals(Alerta.AlertStatus.ALERT)){
            return true;
        }
        return false;
    }

}
