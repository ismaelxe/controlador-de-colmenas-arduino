package com.example.ismael.colmeninfo.Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.ismael.colmeninfo.Entidades.Colmena;
import com.example.ismael.colmeninfo.Entidades.Mensaje;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DBMensajes {

    private static final String dbName = "MENSAJES";
    private static final String [] dbFields = new String[]{"_id", "COLMENA_ID", "MENSAJE_TEXT", "FECHA",
            "ES_RECIBIDO", "LEIDO"};

    private SQLiteDatabase db = null;
    private DBHelper dbHelper = null;
    private Context context;
    private DBColmenas colmenasDao;

    public DBMensajes(Context context){
        this.context = context;
        dbHelper = new DBHelper(context, null);
        db = dbHelper.getWritableDatabase();
    }
    public void close(){
        dbHelper.close();
    }


    public long save(Mensaje sms){
        ContentValues values = new ContentValues();
        values.put("COLMENA_ID", sms.getColmenaID());
        values.put("MENSAJE_TEXT", sms.getMensaje());
        values.put("FECHA", sms.getFecha().toString());
        values.put("ES_RECIBIDO", sms.isEsRecbido());
        values.put("LEIDO", sms.getCachedStatus().ordinal());

        Log.d("DBMensajeAdd", "Mensaje añadido");

        return db.insert("MENSAJES", null, values);
    }

    public List<Mensaje> findMensajesByColmena(Integer colmena){
        return findMensajesByColmena(colmena, null);
    }

    public List<Mensaje> findMensajesByColmena(Integer colmena, String limit){
        if(colmena == null){
            return null;
        }
        List<Mensaje> mensajes = new ArrayList<>();
        Cursor cursor = db.query(dbName, dbFields, "COLMENA_ID = ?", new String[]{String.valueOf(colmena)}, null, null, "_id desc", limit);

        while(cursor.moveToNext()){
            mensajes.add(makeCursorToMensaje(cursor));
        }
        cursor.close();

        return mensajes;
    }

    public List<Mensaje> findAll(){
        List<Mensaje> mensajes = new ArrayList<>();

        Cursor cursor = db.query(dbName,dbFields,null,null,null,null,"_id desc");

        while (cursor.moveToNext()){
            mensajes.add(makeCursorToMensaje(cursor));
        }
        return mensajes;

    }

    private Mensaje makeCursorToMensaje(Cursor cursor) {
        Mensaje mensaje = new Mensaje();
        int idColumn = cursor.getColumnIndex("_id");
        int colmenaId = cursor.getColumnIndex("COLMENA_ID");
        int mensajeText = cursor.getColumnIndex("MENSAJE_TEXT");
        int fecha = cursor.getColumnIndex("FECHA");
        int esRecibido = cursor.getColumnIndex("ES_RECIBIDO");
        int leido = cursor.getColumnIndex("LEIDO");

        mensaje.setId(cursor.getString(idColumn));
        mensaje.setColmenaID(cursor.getInt(colmenaId));
        mensaje.setMensaje(cursor.getString(mensajeText));
        mensaje.setFecha(new Date(cursor.getLong(fecha)));
        mensaje.setEsRecbido(cursor.getInt(esRecibido)==1?true:false);
        mensaje.setCachedStatus(Mensaje.CachedStatus.values()[cursor.getInt(leido)]);

        return  mensaje;
    }


}
