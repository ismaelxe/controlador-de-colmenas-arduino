package com.example.ismael.colmeninfo.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.ismael.colmeninfo.Entidades.Alerta;
import com.example.ismael.colmeninfo.Entidades.Colmena;
import com.example.ismael.colmeninfo.Modelo.DBHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AlertaDao {

    private static final String DBNAME = "ALERTAS";
    private static final String[] COLUMNS = new String[]{"_id", "colmena_id", "type", "fecha", "status"};
    private static final String TAG = "AlertaDao";

    private DBHelper dbHelper;
    private Context context;
    private SQLiteDatabase db;

    public AlertaDao(Context context) {
        this.context = context;
        dbHelper = new DBHelper(context, null);
        db = dbHelper.getWritableDatabase();

    }

    public void close() {
        db.close();
    }

    public void save(List<Alerta> alertas){
        for(Alerta alerta : alertas){
            save(alerta);
        }
    }

    public long save(Alerta alerta) {
        ContentValues values = new ContentValues();
        values.put("colmena_id", alerta.getColmenaId());
        values.put("fecha", alerta.getFecha().getTime());
        values.put("type", alerta.getType().ordinal());
        values.put("status", alerta.getStatus().ordinal());


        int idResult;
        if (alerta.getId() == -1) {
            idResult = (int) db.insert(DBNAME, null, values);
        } else {
            idResult = db.update(DBNAME, values, "_id=?", new String[]{String.valueOf(alerta.getId())});
        }
        Log.d("ConfigurationColmenaDAO", "save=" + idResult);
        return idResult;

    }

    public List<Alerta> findAll(){
        List<Alerta> alertas = new ArrayList<>();
        Cursor c = db.query(DBNAME, COLUMNS, null,
                null, null, null, "_id desc");
        Alerta alerta;
        while (c.moveToNext()) {
            alerta = makeCursorToAlerta(c);

            alertas.add(alerta);
        }
        c.close();

        return alertas;
    }


    public List<Alerta> findByColmena(Colmena colmena){
        return findByColmena(colmena, null);
    }
    public List<Alerta> findByColmena(Colmena colmena, String limit){
        List<Alerta> alertas = new ArrayList<>();
        Cursor c = db.query(DBNAME, COLUMNS, "colmena_id=?",
                new String[]{String.valueOf(colmena.getId())}, null, null, null, limit);
        Alerta alerta;
        while (c.moveToNext()) {
            alerta = makeCursorToAlerta(c);

            alertas.add(alerta);
        }
        c.close();

        return alertas;
    }

    public Alerta findLastByColmenaAndStatus(Colmena colmena, Alerta.AlertType type){
        Cursor c = db.query(DBNAME, COLUMNS, "colmena_id=? AND type=?",
                new String[]{String.valueOf(colmena.getId()), String.valueOf(type.ordinal())}, null, null, "_id desc", "1");
        Alerta alerta = null;
        if (c.moveToNext()) {
            alerta = makeCursorToAlerta(c);

        }
        c.close();

        return alerta;
    }

    private Alerta makeCursorToAlerta(Cursor c) {
        Alerta alerta = new Alerta();

        int idColumn = c.getColumnIndex("_id");
        int idColmenaColumn = c.getColumnIndex("colmena_id");
        int typeColumn = c.getColumnIndex("type");
        int fechaColumn = c.getColumnIndex("fecha");
        int statusColumn = c.getColumnIndex("status");

        alerta.setId(c.getInt(idColumn));
        alerta.setColmenaId(c.getInt(idColmenaColumn));
        alerta.setType(Alerta.AlertType.values()[c.getInt(typeColumn)]);
        alerta.setFecha(new Date(c.getInt(fechaColumn)));
        alerta.setStatus(Alerta.AlertStatus.values()[c.getInt(statusColumn)]);

        return alerta;
    }


}
