Controlador de colmenas Arduino

Mensajes de estado (ST)
	- 0 --> OK (La colmena esta en condiciones optimas, correcta temperatura, humedad, bateria...)
	- 1 --> Sobrecalentamiento ( La colmena ha sobrepasado el limite maximo establecido de temperatura)
	- 2 --> Excesiva humedad ( La colmena ha sobrepasado el limite maximo establecido de humedad)
	- 3 --> Baja bateria ( La colmena entrará en modo ahorro de energia y reducira sus funciones a las basicas)
	- 4 --> Colmena apagada ( Cuando la energia de la colmena este a punto de acabarse enviara un mensaje indicando que se apagara en breve)
	- 5 --> Colmena encendida / Ahorro de bateria desactivado ( Cuando la colmena haya obtenido suficiente energia para encenderse y salir del modo ahorro de energia)
	- 10 --> ALERTA ALARMA ACTIVADA ( El sensor de vibracion se ha activado y es posible que la esten robando)


Mensajes de temperatura (T)
	- 999 --> Ha ocurrido un error o no se ha sincronizado, pruebe a sincronizar
	- -10...60 --> Rango de funcionamiento de sensor ( en realidad es desdede 0º a 60º)

Mensajes de humedad (H)
	- -1 --> Ha ocurrido un error o no se ha sincronizado, pruebe a sincronizar
	- 0 -100 --> Rango de funcionamiento de sensor


ejemplo: ST:0 T:25 H:80 

importante! arduino debe recibir el mensaje con un espacio al final, de lo contrario no separara correctamente la ultima orden