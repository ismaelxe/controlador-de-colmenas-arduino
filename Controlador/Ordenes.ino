String getResultadoOrden(String orden) {
  String comando = "";
  String valorComando = "";
  //  char c;

  //Con este bucle obtenemos por un lado el comando de la orden
  //y por el otro el valorComando de esta
  for (int i = 0; i < orden.length(); i++) {
    char c = orden.charAt(i);
    if (c != ':') {
      comando.concat(c);
    } else {
      i += 1;
      for (; i < orden.length(); i++) {
        c = orden.charAt(i);
        valorComando.concat(c);
      }
    }
  }

  //Una vez obtenidos los valorComandos tenemos que elegir que orden es
  //y que debe hacer con el valorComando introducido
  return procesarOrden(comando, valorComando);
}

String procesarOrden(String comando, String valorComando) {
  Serial.print("Procesando ordenes SMS");
  String resultadoComando;
  //  if (comando == "V") { //Sensor de vibracion (alarma anti-robo)
  //    resultadoComando = procesarOrdenV(valorComando);
  //  }
  if (comando == "T") { //Temperatura
    resultadoComando = procesarOrdenT(valorComando);
  }
  else if (comando == "H") {
    resultadoComando = procesarOrdenH(valorComando);
  }
  else if (comando == "B") {
    resultadoComando = procesarOrdenB(valorComando);
  }
  //Config sensors
  else if (comando == "CT") {
    if (valorComando == "1") {
      temperaturaEstaActivado = true;
      resultadoComando = comando + ":1 ";
    } else {
      temperaturaEstaActivado = false;
      resultadoComando = comando + ":0 ";
    }
  }
  else if (comando == "CH") {
    if (valorComando == "1") {
      humedadEstaActivado = true;
      resultadoComando = comando + ":1 ";
    } else {
      humedadEstaActivado = false;
      resultadoComando = comando + ":0 ";
    }
  }
  else if (comando == "CV") {
    if (valorComando == "1") {
      vibracionEstaActivado = true;
      resultadoComando = comando + ":1 ";
    } else {
      vibracionEstaActivado = false;
      resultadoComando = comando + ":0 ";
    }
  }else if(comando == "SMinT"){
    tempMin = valorComando.toDouble();
    resultadoComando = comando + ":"+ tempMin + " ";
  }else if(comando == "SMaxT"){
    tempMax = valorComando.toDouble();
    resultadoComando = comando + ":"+ tempMax + " ";
  }else if(comando == "SMinT"){
    humedadMin = valorComando.toDouble();
    resultadoComando = comando + ":"+ humedadMin + " ";
  }else if(comando == "SMinH"){
    humedadMax = valorComando.toDouble();
    resultadoComando = comando + ":"+ humedadMax + " ";
  }else if(comando == "SlowB"){
    bateriaNotificacionMin = valorComando.toInt();
    resultadoComando = comando + ":"+ bateriaNotificacionMin + " ";
  }

  return resultadoComando;
}

//Gestor de ordenes de Vibracion
String procesarOrdenV(String valorComando) {
  String resultadoComando = "";
  resultadoComando.concat("V:");

  //Comprovamos los distintos valorComandoes y actuamos acorde
  if (valorComando == "?") {
    resultadoComando.concat(vibracionAlerta ? '1' : '0');
  }
  resultadoComando.concat(" ");

  return resultadoComando;
}

//Gestor de ordenes de Temperatura
String procesarOrdenT(String valorComando) {
  String resultadoComando = "";
  double temp = getTemp();
  resultadoComando.concat("T:");

  //Comprovamos los distintos valorComandoes y actuamos acorde
  resultadoComando.concat(temp);

  resultadoComando.concat(" ");
  if (temp > tempMax) {
    temperaturaHighAlerta = true;
  }
  if (temp < tempMin) {
    temperaturaLowAlerta = false;
  }
  return resultadoComando;
}


//Gestor de ordenes de Humedad
String procesarOrdenH(String valorComando) {
  String resultadoComando = "";
  double hum = getHumidity();
  resultadoComando.concat("H:");

  //Comprovamos los distintos valorComandoes y actuamos acorde
  if (valorComando == "?") {
    resultadoComando.concat(hum);
  }

  resultadoComando.concat(" ");

  if(hum > humedadMax){
    humedadHighAlerta = true;
  }
  if(hum < humedadMin){
    humedadLowAlerta = true;
  }

  return resultadoComando;
}

//Gestor de ordenes de Bateria
String procesarOrdenB(String valorComando) {
  String resultadoComando = "";
  int batery = getBatery();
  resultadoComando.concat("B:");

  //Comprobamos los distintos valorComandoes y actuamos acorde
  if (valorComando == "?") {
    resultadoComando.concat(batery);
  }

  resultadoComando.concat(" ");

  if(batery <= bateriaNotificacionMin){
    bateriaBajaAlerta = true;
  }

  return resultadoComando;
}
