
long tiempoEntreProcesamientoAlertasInMillis = 24000; //Un minuto  //6 seg
long tiempoEntreAlertasDeVibracion = 24000;  //30 minutos


unsigned long nextFire = 0;
unsigned long nextVibrationFire = 0;

void shouldCheckVibracion(){

if(millis() > nextVibrationFire && checkVibracion()){
    nextVibrationFire = millis() + tiempoEntreAlertasDeVibracion;
    vibracionAlerta = true;
  }
}

void iniciarAlertas() {
  if (millis() > nextFire) {
    nextFire = millis() + tiempoEntreProcesamientoAlertasInMillis;
    String mensaje = procesarTimers();
    if(mensaje != ""){
      enviarSMS(mensaje);
    }
  }
}

String procesarTimers(){
  Serial.print("procesando alertas");
    temperaturaTimer();
    humedadTimer();
    bateryTimer();

    //Buscamos las alertas activadas y las enviamos
    return procesarAlertasTimer();
  }


String procesarAlertasTimer() {
  String mensajeAlerta = "";
  
  if(vibracionAlerta){
    mensajeAlerta.concat("A:V ");
    vibracionAlerta = false;  
  }
  if (temperaturaHighAlerta) {
    mensajeAlerta.concat("A:TX ");
    temperaturaHighAlerta = false;
  }
  if (temperaturaLowAlerta) {
    mensajeAlerta.concat("A:TN ");
    temperaturaLowAlerta = false;
  }

  if (humedadHighAlerta) {
    mensajeAlerta.concat("A:HX ");
    temperaturaHighAlerta = false;
  }
  if (humedadLowAlerta) {
    mensajeAlerta.concat("A:HN ");
    temperaturaLowAlerta = false;
  }
  if(bateriaBajaAlerta){
    mensajeAlerta.concat("A:BN ");
    bateriaBajaAlerta = false;
  }

  return mensajeAlerta;


}

void temperaturaTimer() {
  if (!temperaturaEstaActivado) { //Si no esta activado terminamos la funcion
    return;
  }
  checkTemperaturaAlerta();

}

void humedadTimer() {
  if (!humedadEstaActivado) {
    return;
  }
  checkHumedadAlerta();

}
void bateryTimer(){
  //TODO este metodo deberia calcular la bateria
  
  if(getBatery() <= bateriaNotificacionMin){
    bateriaBajaAlerta = true;
  }
}


//Comprobamos la temperatura y activamos la alerta
void checkTemperaturaAlerta() {
  temperatura = getTemp();
  if (temperatura >= tempMax) {
    temperaturaHighAlerta = true;
  } else {
    if (temperatura <= tempMin) {
      temperaturaLowAlerta = true;
    }
  }
}

void checkHumedadAlerta() {
  humedad = getHumidity();
}
