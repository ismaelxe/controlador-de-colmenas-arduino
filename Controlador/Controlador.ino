//#include <TimedAction.h>

#include <dht.h>
#include <SoftwareSerial.h>         //Incluimos la libreria SoftwareSerial


//Pines y sensores
//SoftwareSerial sim800l(0, 1);       //Declaramos los pines RX(8) y TX(9)(en mega) que vamos a usar (0,1 en caso de nano)
#define DHT11_PIN 7
#define vibracion_pin 2
dht DHT;

//Variables sim800l
String NUMSTEL[5];

//Variables por defecto de los sensores
double tempMax = 20;
double tempMin = 5;

double humedadMax = 80;
double humedadMin = 20;

int bateriaNotificacionMin = 20;

int limiteVibracion = 1000;

//estado
int estado;  //esta variable sera eliminada en proximas mejoras


//Sensores activos, por defecto todos estan activos
boolean vibracionEstaActivado = true;
boolean temperaturaEstaActivado = true;
boolean humedadEstaActivado = true;

//Alarmas activadas, al principio todas las alarmas desactivadas
boolean vibracionAlerta = false;
boolean temperaturaHighAlerta = false;
boolean temperaturaLowAlerta = false;
boolean humedadHighAlerta = false;
boolean humedadLowAlerta = false;
boolean bateriaBajaAlerta = false;

//Datos de los sensores
double temperatura;
double humedad;
int bateria;


//unsigned long delta = 0;  //esta variable almacena el tiempo ded milisegundos de un determinado momento

//mensajes
String smsSaliente;
String smsEntrante;

//pruebas
boolean dev = true;



void setup() {
  NUMSTEL[0] = "634628705";
  pinMode(vibracion_pin, INPUT);
    attachInterrupt(digitalPinToInterrupt(vibracion_pin), shouldCheckVibracion, HIGH);
  Serial.begin(9600);
//  initGSM();
}
////////////////////////////////////////////////////////////////////////////////////////////////
void loop() {
  if (Serial.available()) {
    smsEntrante = leerMensaje();
Serial.print(smsEntrante);
    smsSaliente = "";
    String orden = "";
    for (int i = 0; i < smsEntrante.length(); i++) {
      char c = smsEntrante.charAt(i);
      if (c == ' ') {
        smsSaliente.concat(getResultadoOrden(orden));
        orden = "";
      } else {
        orden.concat(c);
      }
    }
    enviarSMS(smsSaliente);
  }

  iniciarAlertas();

 
}
////////////////////////////////////////////////////////////////////////////////////////////////




