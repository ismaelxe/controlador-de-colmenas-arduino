const int ERR_TEMP = -99;
const int ERR_HUM = -1;



boolean checkVibracion() {
  if ((pulseIn (vibracion_pin, HIGH) > limiteVibracion)) {
    vibracionAlerta = true;
    return true;
  }
  return false;
}


//Obtener temperatura
double getTemp() {
  int temp;
  int i = 0;

  while (i < 5) {
    if (comprobarSensorDHT()) {
      return DHT.temperature;
      i = 3;
    }
    i++;
  }

  return temp;
}


//Obtener humedad
double getHumidity() {
  int hum;
  int i = 0;

  while (i < 5) {
    if (comprobarSensorDHT()) {
      hum = DHT.humidity;
      return hum;
    }
    i++;
    
  }
  return ERR_HUM;
  
}

int getBatery(){
  if(dev){
    return 80;
  }
}



//Intentamos leer el sensor ya que solo se puede leer una vez por segundo
//en caso de que de error la primera lectura esperaremos un segundo y lo reintentaremos
boolean comprobarSensorDHT() {
  int chk = DHT.read11(DHT11_PIN);
  switch (chk)
  {
    case DHTLIB_OK:
      //No hacemos nada
      return true;
      break;
    default:
//      Serial.print("Unknown error,\t");
      delay(random(1000, 2000));
      return false;
      break;
  }
}
